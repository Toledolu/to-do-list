# Luso Digital Assets Test

Create a simple To Do application with the following requirements:

- Task Model with two fields (title, status).
- Title must be a varchar 255 and Status Enum with values ('pending', 'completed').
- System need to be written in Laravel and Vuejs.
- Implement the CRUD operations to manage the tasks.

## Getting Started

### Prerequisites

You must have PHP (version >= 8.1), Composer and NPM installed in your machine.

### Installing

**1:** Open the cmd interface and go to the path you saved the project folder.

**2:** Execute the command 'composer install' in the cmd.

**3:** After finish execute the command 'npm install' in the cmd.

**4:** Create a file named '.env' into the project root, then copy and paste everything from file '.env.example'.

**5:** Still on the '.env' file, change the Database configs to match the one configured in your PC.

Configuration example:

```
    'DB_HOST' => '192.168.10.10',
    'DB_USERNAME' => 'homestead',
    'DB_PASSWORD' => 'secret',
    'DB_NAME' => 'task_manager',
    'DB_PORT' => '3306',
```

**6:** Execute the command 'php artisan key:generate' in the cmd.

**7:** In the cmd run the command 'php artisan migrate' to create all tables required.

**7.5:** If you wish you can run 'php artisan db:seed' in the cmd to populate the tables with some data.

**8:** In the cmd run the command 'npm run prod'.

**9:** Execute the command 'php artisan serve' in the cmd to launch a local server in the following address 'localhost:8000'.

Now you can access the project by opening your browser and typing 'localhost:8000' at the address bar.

## Authors

* **Lucas Toledo Heilig** - [Toledolu](https://gitlab.com/Toledolu)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
