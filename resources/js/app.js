import './bootstrap';

import Vue from "vue";
import VueSweetalert2 from 'vue-sweetalert2';

import ToDoList from "./components/ToDoList.vue";

Vue.use(VueSweetalert2);

Vue.component('to-do-list', ToDoList);

const app = new Vue({
  el: '#app'
});