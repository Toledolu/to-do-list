@extends('layouts.content')

@section('content')
    
    <to-do-list :data="{{json_encode($tasks)}}"></to-do-list>

@endsection