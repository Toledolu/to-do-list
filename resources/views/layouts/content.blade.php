@extends('layouts.base')

@section('body')
  @include('layouts.menu')

  @yield('content')

  @include('layouts.footer')
@endsection