<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <meta name="csrf-token" content="{{csrf_token()}}">

      @stack('styles')
      <link href="{{asset('css/reset.css')}}" rel="stylesheet">
      <link href="{{asset('css/app.css')}}?v={{microtime()}}" rel="stylesheet">

      <title>Lucas Toledo - To Do List</title>
      <meta name="description" content=""/>
      <meta property="og:type" content="website"/>
      <meta property="og:title" content="Lucas Toledo - To Do List"/>
      <meta property="og:description" content=""/>
      <meta property="og:url" content="{{request()->fullUrl()}}"/>
      <meta property="og:site_name" content="Lucas Toledo - To Do List"/>

      <!-- Global site tag (gtag.js) - Google Analytics -->

      @stack('head')
    </head>
    <body>

      <div id="app" class="d-flex background">
        @yield('body')
      </div>

      <script src="{{asset('js/app.js')}}"></script>
      @stack('scripts')

    </body>
</html>

