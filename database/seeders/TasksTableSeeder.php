<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(app()->environment() == 'local') {
            DB::table('tasks')->truncate();
            DB::table('tasks')->insert([
                [
                    'id' => 1,
                    'title' => 'Study japanese',
                    'status' => 'pending',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ],
                [
                    'id' => 2,
                    'title' => 'Order food',
                    'status' => 'completed',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ],
                [
                    'id' => 3,
                    'title' => 'Clean the house',
                    'status' => 'pending',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ],
                [
                    'id' => 4,
                    'title' => 'Take the trash out',
                    'status' => 'pending',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ],
                [
                    'id' => 5,
                    'title' => 'Go to the movies to watch Oppenheimer',
                    'status' => 'pending',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ],
            ]);
        }
    }
}
