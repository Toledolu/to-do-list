<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Enums\TaskStatusEnum;

class Task extends Model
{
    use HasFactory;
    use softDeletes;

    protected $table = 'tasks';

    protected $fillable = [ 
        'title', 'status'
    ];

    protected $casts = [
        'status' => TaskStatusEnum::class
    ];
}
