<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Enums\TaskStatusEnum;
use Illuminate\Validation\Rules\Enum;

class TaskController extends Controller
{
    public function index() {        
        $tasks = Task::orderBy('id', 'asc')->get();

        return view('home', compact('tasks'));
    }

    public function makeValidation(Request $fields) {
        return $fields->validate([
            'title' => 'required|max:255',
            'status' => [new Enum(TaskStatusEnum::class)],
        ]);
    }

    public function create(Request $request) {
        $validated = $this->makeValidation($request);

        $task = new Task();
        $task->fill($request->all());
        $task->save();

        return response()->json([
            'message' => 'Task created successfully.',
            'obj' => $task,
            'error' => false
        ]);
    }

    public function update(Request $request) {
        $validated = $this->makeValidation($request);

        $task = Task::where('id', $request['id'])->first();

        if(!$task) {
            return response()->json([
                'message' => 'Task not found.',
                'error' => true
            ]);
        }

        $task->fill($request->all());
        $task->save();

        return response()->json([
            'message' => 'Task updated successfully.',
            'obj' => $task,
            'error' => false
        ]);
    }

    public function delete(Request $request) {
        if(!$request->has('id')) {
            return response()->json([
                'message' => 'You need to send the task id.',
                'error' => true
            ]);
        }

        $task = Task::where('id', $request['id'])->first();
        
        if(!$task) {
            return response()->json([
                'message' => 'Task not found.',
                'error' => true
            ]);
        }

        $task->delete();

        return response()->json([
            'message' => 'Task deleted successfully.',
            'obj' => $task,
            'error' => false
        ]);
    }
}
