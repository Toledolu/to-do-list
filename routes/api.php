<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/', 'App\Http\Controllers\TaskController@create')->name('task.create');
Route::put('/', 'App\Http\Controllers\TaskController@update')->name('task.update');
Route::delete('/', 'App\Http\Controllers\TaskController@delete')->name('task.delete');